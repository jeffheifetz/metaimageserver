module Api
  module V1
    class ImagesController < ActionController::Base

      DEFAULT_COUNT = 25

      # This is going to be the index, and since we will be adding data into the DB in the background, this means its very depdendant on timestamps.
      #
      # To accomodate this we'll be using before/after for pagination
      #
      # If I had more time I'd add proper pagination headers
      def index

        Fetch500pxImagesJob.perform_later
        FetchImgurImagesJob.perform_later

        item_count = if params[:count].present?
                       params[:count]
                     else
                       DEFAULT_COUNT
                     end
        render json: Image.limit(item_count).order(timestamp: :asc), status: :ok
      end

      def show
        Fetch500pxImagesJob.perform_later
        FetchImgurImagesJob.perform_later

        render json: Image.find(params[:id]), status: :ok
      end
    end
  end
end
