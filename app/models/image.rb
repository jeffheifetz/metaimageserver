class Image < ActiveRecord::Base
  has_many :comments
  validates :url, uniqueness: true
end
