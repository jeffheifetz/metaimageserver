class FetchImgurImagesJob < ActiveJob::Base
  queue_as :default

  def perform()
    resp = HTTParty.get('https://api.imgur.com/3/gallery/hot/viral/0.json', headers: auth_header)
    resp.parsed_response['data'].each do |image|
      # {"id"=>"kQEyT", "title"=>"How to properly give a massage", "description"=>nil, "datetime"=>1449690406,
      # "cover"=>"Tp8cCH9", "cover_width"=>393, "cover_height"=>579, "account_url"=>"theonewhoknots", "account_id"=>26217868,
      # "privacy"=>"public", "layout"=>"blog", "views"=>155843, "link"=>"http://imgur.com/a/kQEyT", "ups"=>11882, "downs"=>303,
      # "points"=>11579, "score"=>11578, "is_album"=>true, "vote"=>nil, "favorite"=>false, "nsfw"=>false, "section"=>"mildlyinteresting",
      # "comment_count"=>710, "comment_preview"=>nil, "topic"=>"The More You Know", "topic_id"=>11, "images_count"=>17}
      Image.create(url: image['link'], username: image['account_url'], view_count: image['score'],
                   like_count: image['ups'], caption: image['title'], timestamp: Time.at(image['datetime']))
    end
  end

   def auth_header
     { 'Authorization' => 'Client-ID ' + Rails.application.secrets['imgur_client_id'] }
   end
end
