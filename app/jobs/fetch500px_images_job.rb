class Fetch500pxImagesJob < ActiveJob::Base
  queue_as :default

  BASE_URL = 'https://api.500px.com/v1/photos'
  def perform(*args)
    resp = HTTParty.get(BASE_URL, query: query)
    resp.parsed_response['photos'].each do |image|
      # {"id"=>131810423, "user_id"=>14409523, "name"=>"Rufous Backed Shrike", "description"=>"Description.", "camera"=>"Canon EOS 700D",
      # "lens"=>"EF-S55-250mm f/4-5.6 IS II", "focal_length"=>"250", "iso"=>"200", "shutter_speed"=>"1/500", "aperture"=>"6.3",
      # "times_viewed"=>25, "rating"=>80.5, "status"=>1, "created_at"=>"2015-12-09T22:46:20-05:00", "category"=>11, "location"=>nil,
      # "latitude"=>22.6396795490486, "longitude"=>88.4859466552734, "taken_at"=>"2015-12-05T09:36:20-05:00", "hi_res_uploaded"=>0,
      # "for_sale"=>false, "width"=>2469, "height"=>1389, "votes_count"=>15, "favorites_count"=>4, "comments_count"=>0, "nsfw"=>false,
      # "sales_count"=>0, "for_sale_date"=>nil, "highest_rating"=>80.5, "highest_rating_date"=>"2015-12-09T23:12:48-05:00",
      # "license_type"=>0, "converted"=>27, "collections_count"=>4, "crop_version"=>2, "privacy"=>false,
      # "image_url"=>"https://drscdn.500px.org/photo/131810423/w%3D70_h%3D70/d05b81073b15f7e952371c6a33e43a0d?v=2",
      # "images"=>[{"size"=>1, "url"=>"https://drscdn.500px.org/photo/131810423/w%3D70_h%3D70/d05b81073b15f7e952371c6a33e43a0d?v=2", "https_url"=>"https://drscdn.500px.org/photo/131810423/w%3D70_h%3D70/d05b81073b15f7e952371c6a33e43a0d?v=2", "format"=>"jpeg"}], "url"=>"/photo/131810423/rufous-backed-shrike-by-avijit-chowdhury", "positive_votes_count"=>15, "converted_bits"=>27, "store_download"=>false, "store_print"=>false, "store_license"=>false, "request_to_buy_enabled"=>true, "license_requests_enabled"=>true, "watermark"=>false, "image_format"=>"jpeg", "user"=>{"id"=>14409523, "username"=>"avibarca10", "firstname"=>"Avijit", "lastname"=>"Chowdhury", "city"=>"kolkata", "country"=>"India", "usertype"=>0, "fullname"=>"Avijit Chowdhury", "userpic_url"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/1.jpg?1", "userpic_https_url"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/1.jpg?1", "cover_url"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/cover_2048.jpg?3", "upgrade_status"=>0, "store_on"=>true, "affection"=>399, "avatars"=>{"default"=>{"https"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/1.jpg?1"}, "large"=>{"https"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/2.jpg?1"}, "small"=>{"https"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/3.jpg?1"}, "tiny"=>{"https"=>"https://pacdn.500px.org/14409523/ae8dc83d7a3b86748ecba37e67cbd5714bf98019/4.jpg?1"}}}, "licensing_requested"=>false}
    Image.create(url: image['image_url'], username: image['user']['username'], view_count: image['times_viewed'],
                 like_count: image['favorites_count'], caption: image['name'], timestamp: image['created_at'])
    end
  end

  def query
    { feature: :popular, sort: :created_at, image_size: 1, include_store: :store_download, include_states: :voted, consumer_key: Rails.application.secrets['500px_consumer_key'] }
  end
end
