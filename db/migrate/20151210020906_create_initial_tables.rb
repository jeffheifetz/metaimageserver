class CreateInitialTables < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :url, null: false
      t.string :username
      t.integer :view_count, default: 0, null: false
      t.integer :like_count, default: 0, null: false
      t.string :caption
      t.datetime :timestamp
      t.timestamps null: false
    end

    create_table :comments do |t|
      t.belongs_to :image, index: true
      t.string :comment
      t.string :username
      t.datetime :timestamp
      t.timestamps null: false
    end

    add_index :images, [:timestamp]
    add_index :images, [:url], unique: true
  end
end
