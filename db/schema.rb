# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151210020906) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.integer  "image_id"
    t.string   "comment"
    t.string   "username"
    t.datetime "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["image_id"], name: "index_comments_on_image_id", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "url",                    null: false
    t.string   "username"
    t.integer  "view_count", default: 0, null: false
    t.integer  "like_count", default: 0, null: false
    t.string   "caption"
    t.datetime "timestamp"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "images", ["timestamp"], name: "index_images_on_timestamp", using: :btree
  add_index "images", ["url"], name: "index_images_on_url", unique: true, using: :btree

end
