== README

This is a basic rails server only. I know its unlikely to be the technology you wanted to see,
but I was feeling very time tight with this so I went with what I've been working in for the last
year.

==Setup

Basic ruby setup

* bundle install
* rake db:create
* rake db:migrate
* rails s -p 30002 (Becuase the client is using 3000 and 3001)

==Loading Data

While I have the data loading mechanisms in background jobs so they won't slow down API calls, I
never put in the work to schedule them on startup and continuously in the background. To warm the data
just make a GET (or open in browser) to localhost:3002/api/v1/images

